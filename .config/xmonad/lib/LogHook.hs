module LogHook
  ( myLogHook
  ) where

import XMonad

myLogHook :: X ()
myLogHook = return ()
